package com.example.mygame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {
    private var resultation = 0
    private var resultation2 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        visible()
    }

    /*გადაადგილების (ნაბიჯების) ღილაკების ხილვადობა*/
    private fun visible() {
        step1.isInvisible = true
        step2.isInvisible = true
        step3.isInvisible = true
        step4.isInvisible = true
        step5.isInvisible = true
        step6.isInvisible = true
        step7.isInvisible = true
        step8.isInvisible = true
        step9.isInvisible = true
        step10.isInvisible = true
        step11.isInvisible = true
        step12.isInvisible = true
        step13.isInvisible = true
        step14.isInvisible = true
        step15.isInvisible = true
        step16.isInvisible = true
        step17.isInvisible = true
        step18.isInvisible = true
        step19.isInvisible = true
        step20.isInvisible = true
        step21.isInvisible = true
        step22.isInvisible = true
        step23.isInvisible = true
        step24.isInvisible = true
        step25.isInvisible = true
        step26.isInvisible = true
        step27.isInvisible = true

        steps1.isInvisible = true
        steps2.isInvisible = true
        steps3.isInvisible = true
        steps4.isInvisible = true
        steps5.isInvisible = true
        steps6.isInvisible = true
        steps7.isInvisible = true
        steps8.isInvisible = true
        steps9.isInvisible = true
        steps10.isInvisible = true
        steps11.isInvisible = true
        steps12.isInvisible = true
        steps13.isInvisible = true
        steps14.isInvisible = true
        steps15.isInvisible = true
        steps16.isInvisible = true
        steps17.isInvisible = true
        steps18.isInvisible = true
        steps19.isInvisible = true
        steps20.isInvisible = true
        steps21.isInvisible = true
        steps22.isInvisible = true
        steps23.isInvisible = true
        steps24.isInvisible = true
        steps25.isInvisible = true
        steps26.isInvisible = true
        steps27.isInvisible = true
    }

    /*ღილაკების გააქტიურება და ფუნქციების მინიჭება*/
    private fun init() {
        randomButton1.setOnClickListener {
            dajameba()
            checkWinner(randomButton1)
        }
        random1.setOnClickListener {
            dajameba()
            checkWinner(random1)
        }
        randomButton2.setOnClickListener {
            dajameba2()
            checkWinner(randomButton2)
        }
        random2.setOnClickListener {
            dajameba2()
            checkWinner(random2)
        }

        reastartButton.setOnClickListener {
            restart()
            visible()
        }
    }

    /*თამაშის თავიდან დაწყების გასააქტიურებელი ღილაკი*/
    private fun restart() {
        random1.text = "0"
        random2.text = "0"
        randomButton1.isClickable = true
        randomButton2.isClickable = true
        random1.isClickable = true
        random2.isClickable = true
        resultation = 0
        resultation2 = 0
    }

    /*სპინის შედეგი და დაჯამება პირველი მოთამაშისთვის*/
    private fun dajameba() {
        var result = 0
        val randomNumber = Random().nextInt(4) + 1

        random1.text = randomNumber.toString()

        resultation += randomNumber
        result = resultation

        if (result == 1) {
            step1.isVisible = true
        } else if (result == 2) {
            step1.isVisible = true
            step2.isVisible = true
        } else if (result == 3) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
        } else if (result == 4) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
        } else if (result == 5) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
        } else if (result == 6) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
        } else if (result == 7) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true

        } else if (result == 8) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true

        } else if (result == 9) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
        } else if (result == 10) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
        } else if (result == 11) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
        } else if (result == 12) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
        } else if (result == 13) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
        } else if (result == 14) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
        } else if (result == 15) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
        } else if (result == 16) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
        } else if (result == 17) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
        } else if (result == 18) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
        } else if (result == 19) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
        } else if (result == 20) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
        } else if (result == 21) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
        } else if (result == 22) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
            step22.isVisible = true
        } else if (result == 23) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
            step22.isVisible = true
            step23.isVisible = true
        } else if (result == 24) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
            step22.isVisible = true
            step23.isVisible = true
            step24.isVisible = true
        } else if (result == 25) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
            step22.isVisible = true
            step23.isVisible = true
            step24.isVisible = true
            step25.isVisible = true
        } else if (result == 26) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
            step22.isVisible = true
            step23.isVisible = true
            step24.isVisible = true
            step25.isVisible = true
            step26.isVisible = true
        } else if (result <= 31) {
            step1.isVisible = true
            step2.isVisible = true
            step3.isVisible = true
            step4.isVisible = true
            step5.isVisible = true
            step6.isVisible = true
            step7.isVisible = true
            step8.isVisible = true
            step9.isVisible = true
            step10.isVisible = true
            step11.isVisible = true
            step12.isVisible = true
            step13.isVisible = true
            step14.isVisible = true
            step15.isVisible = true
            step16.isVisible = true
            step17.isVisible = true
            step18.isVisible = true
            step19.isVisible = true
            step20.isVisible = true
            step21.isVisible = true
            step22.isVisible = true
            step23.isVisible = true
            step24.isVisible = true
            step25.isVisible = true
            step26.isVisible = true
            step27.isVisible = true
        }
    }

    /*სპინის შედეგი და დაჯამება მეორე მოთამაშისთვის*/
    private fun dajameba2() {
        var result2 = 0
        val randomNumber = Random().nextInt(4) + 1

        random2.text = randomNumber.toString()

        resultation2 += randomNumber
        result2 = resultation2

        if (result2 == 1) {
            steps1.isVisible = true
        } else if (result2 == 2) {
            steps1.isVisible = true
            steps2.isVisible = true
        } else if (result2 == 3) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
        } else if (result2 == 4) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
        } else if (result2 == 5) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
        } else if (result2 == 6) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
        } else if (result2 == 7) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true

        } else if (result2 == 8) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true

        } else if (result2 == 9) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
        } else if (result2 == 10) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
        } else if (result2 == 11) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
        } else if (result2 == 12) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
        } else if (result2 == 13) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
        } else if (result2 == 14) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
        } else if (result2 == 15) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
        } else if (result2 == 16) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
        } else if (result2 == 17) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
        } else if (result2 == 18) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
        } else if (result2 == 19) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
        } else if (result2 == 20) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
        } else if (result2 == 21) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
        } else if (result2 == 22) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
            steps22.isVisible = true
        } else if (result2 == 23) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
            steps22.isVisible = true
            steps23.isVisible = true
        } else if (result2 == 24) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
            steps22.isVisible = true
            steps23.isVisible = true
            steps24.isVisible = true
        } else if (result2 == 25) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
            steps22.isVisible = true
            steps23.isVisible = true
            steps24.isVisible = true
            steps25.isVisible = true
        } else if (result2 == 26) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
            steps22.isVisible = true
            steps23.isVisible = true
            steps24.isVisible = true
            steps25.isVisible = true
            steps26.isVisible = true
        } else if (result2 <= 31) {
            steps1.isVisible = true
            steps2.isVisible = true
            steps3.isVisible = true
            steps4.isVisible = true
            steps5.isVisible = true
            steps6.isVisible = true
            steps7.isVisible = true
            steps8.isVisible = true
            steps9.isVisible = true
            steps10.isVisible = true
            steps11.isVisible = true
            steps12.isVisible = true
            steps13.isVisible = true
            steps14.isVisible = true
            steps15.isVisible = true
            steps16.isVisible = true
            steps17.isVisible = true
            steps18.isVisible = true
            steps19.isVisible = true
            steps20.isVisible = true
            steps21.isVisible = true
            steps22.isVisible = true
            steps23.isVisible = true
            steps24.isVisible = true
            steps25.isVisible = true
            steps26.isVisible = true
            steps27.isVisible = true
        }
    }

    /*გამარჯვებულის გამოვლენა*/
    private fun checkWinner(button: Button) {
        if (step27.isVisible) {
            Toast.makeText(this, "The Winner is Player 1", Toast.LENGTH_SHORT).show()
            randomButton1.isClickable = false
            randomButton2.isClickable = false
            random1.isClickable = false
            random2.isClickable = false
        } else if (steps27.isVisible) {
            Toast.makeText(this, "The Winner is Player 2", Toast.LENGTH_SHORT).show()
            randomButton1.isClickable = false
            randomButton2.isClickable = false
            random1.isClickable = false
            random2.isClickable = false
        }
    }
}


